class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def courses
    @courses
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    @courses << new_course unless @courses.include?(new_course)
  end

  def courses
    @courses
  end


  def course_load
    @load_courses = {}
    @courses.each{|x|
      if @load_courses.keys.include?(x.department)
        @load_courses[x.department] += x.credits
      else
        @load_courses[x.department] = x.credits
      end
    }
    @load_courses
  end
end
